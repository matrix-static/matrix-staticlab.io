#! /usr/bin/env node

import {
	readConfig,
	cleanOutputFolder,
	createOutputFolder,
} from "./server-config.js";

import {
	createWellKnownMatrixServer,
	createMatrixFederationFolders,
	createRedirectsFile,
	createPublicRoomsFiles,
	createDirectoryRoomAliasFiles,
	createDirectoryRoomIdState,
	createDirectoryRoomIdMessages,
	createJoinRoomReponses,
	createIndexHtml,
} from "./server-files.js";

import { readContentFolder, readBuildFolder } from "./server-content.js";
import { contentFolderToMX } from "./mx-sdk.js";

const init = async (args) => {
	// get user parameters from script arguments
	const params = args.slice(2);

	// read the project config file
	const { config, error } = await readConfig();
	if (error || !config) {
		console.log(error);
		return;
	}

	const outputFolder = "build";

	// delete and crea the `./build/` folder
	await cleanOutputFolder(outputFolder);
	await createOutputFolder(outputFolder);

	// create a build output folder, with the matrix static files
	// create the .well-known/matrix/server.json file
	await createWellKnownMatrixServer(config);

	// create the _redirects files for the gitlab, netlify etc. pages
	await createRedirectsFile();

	// create the _matrix/federation/v1/ folder
	await createMatrixFederationFolders(config);

	// read the ./content/ folder structure
	const content = await readContentFolder(config);
	if (!content) {
		console.info("No data in the ./content/ folder");
		return;
	}

	const mxContent = contentFolderToMX({ config, content });

	// create API "spaces/rooms/events" from ./content/ folder/file structure
	await createPublicRoomsFiles(mxContent);

	// create API _matrix/client/r0/directory/room(s)/(:id,:alias)/*
	await createDirectoryRoomAliasFiles(mxContent);

	// create _matrix/client/r0/directory/rooms/{id}:{domain}.{tld}/{messages,state}.json
	await createDirectoryRoomIdState(mxContent);
	await createDirectoryRoomIdMessages(mxContent);

	// create the endpoints to fake "joining a room"
	await createJoinRoomReponses(mxContent);

	// read the build folder output, for index.html creation
	const build = await readBuildFolder();
	if (build.tree) {
		await createIndexHtml(build.tree, config);
	}

	// log output
	console.info("Built!" /* params, config, content */);
};

init(process.argv);
