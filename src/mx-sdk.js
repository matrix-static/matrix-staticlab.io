const ROOM_VERSION = "9";

const getServerInfo = (serverURL) => {
	if (!serverURL) return;
	const protocol = "https";
	const { hostname } = new URL(`${protocol}://${serverURL}`);
	return {
		port: 443,
		hostname,
		homeserver: hostname,
	};
};

const contentFolderToMX = ({ content, config }) => {
	const { homeserver } = getServerInfo(config["m.server"]);
	const roomsContent = Object.entries(content.rooms || {});
	const spacesContent = Object.entries(content.spaces || {});
	const eventsContent = Object.entries(content.events || {});

	const rooms = roomsContent.map(([roomId, roomData]) => {
		return new MXRoom(roomData, homeserver);
	});

	const spaces = spacesContent.map(([spaceId, spaceData]) => {
		return new MXSpace(spaceData, homeserver);
	});

	const events = eventsContent.map(([eventId, eventData]) => {
		return new MXEvent(eventData, homeserver);
	});

	return {
		rooms,
		spaces,
		events,
	};
};

const toPublicRoom = ({
	homeserver,
	props,
	type = "m.room" || "m.space",
	room_id,
}) => {
	const { localAlias } = props;
	const server = homeserver;
	return {
		avatar_url: `mxc://${server}/${localAlias}`,
		canonical_alias: `#${localAlias}:${server}`,
		guest_can_join: true,
		join_rule: "public",
		name: localAlias,
		num_joined_members: 0,
		room_id,
		room_type: type,
		topic: `A test static ${type} for ${localAlias}`,
		world_readable: true,
	};
};

const toDirectoryRoom = ({ homeserver, props, room_id }) => {
	const server = homeserver;
	return {
		room_id,
		servers: [homeserver],
	};
};

const buildEventId = ({ localPath, homeserver, localAlias }) => {
	const id = `${localPath}/${localAlias}:${homeserver}`;
	return "$" + btoa(id);
};

const buildRoomId = ({ localPath, homeserver }) => {
	const id = `!${btoa(localPath)}:${homeserver}`;
	return id;
};

const buildCanonicalAlias = ({ localAlias, homeserver }) => {
	const alias = `#${localAlias}:${homeserver}`;
	return alias;
};

const roomToStateEvents = (roomOrSpace) => {
	const { homeserver, room_id, canonical_alias } = roomOrSpace;
	const user = `@admin:${homeserver}`;
	const date = Date.now();
	const events = [
		{
			type: "m.room.name",
			content: {
				name: canonical_alias,
			},
		},
		{
			type: "m.room.history_visibility",
			content: {
				history_visibility: "world_readable",
			},
		},
		{
			type: "m.room.topic",
			content: {
				topic: `Example topic ${canonical_alias}`,
			},
		},
		{
			type: "m.room.join_rules",
			content: { join_rule: "public" },
		},
		{
			type: "m.room.create",
			content: { creator: user, room_version: ROOM_VERSION },
		},
		{
			type: "m.room.canonical_alias",
			content: { alias: canonical_alias },
		},
	];
	return events.map((eventData) => {
		eventData.user_id = user;
		eventData.sender = user;
		eventData.event_id = btoa(`${date}-${eventData.type}-${room_id}`);
		eventData.origin_server_ts = date;
		return eventData;
	});
};

class MXRoom {
	constructor(content, homeserver) {
		this.props = content;
		this.homeserver = homeserver;
		this.room_id = buildRoomId({
			homeserver: this.homeserver,
			localPath: this.props.localPath,
		});
		this.canonical_alias = buildCanonicalAlias({
			homeserver: this.homeserver,
			localAlias: this.props.localAlias,
		});
		this.canonical_alias_encoded = encodeURIComponent(this.canonical_alias);
	}
	toPublicRoom() {
		return toPublicRoom({
			props: this.props,
			homeserver: this.homeserver,
			type: "m.room",
			room_id: this.room_id,
		});
	}
	toDirectoryRoom() {
		return toDirectoryRoom({
			props: this.props,
			homeserver: this.homeserver,
			room_id: this.room_id,
		});
	}
	toStateEvents() {
		return roomToStateEvents(this);
	}
}

class MXSpace {
	constructor(content, homeserver) {
		this.props = content;
		this.homeserver = homeserver;
		this.room_id = buildRoomId({
			homeserver: this.homeserver,
			localPath: this.props.localPath,
		});
		this.canonical_alias = buildCanonicalAlias({
			homeserver: this.homeserver,
			localAlias: this.props.localAlias,
		});
		this.canonical_alias_encoded = encodeURIComponent(this.canonical_alias);
	}
	toPublicRoom() {
		return toPublicRoom({
			props: this.props,
			homeserver: this.homeserver,
			type: "m.space",
			room_id: this.room_id,
		});
	}
	toDirectoryRoom() {
		return toDirectoryRoom({
			props: this.props,
			homeserver: this.homeserver,
			room_id: this.room_id,
		});
	}
	toStateEvents() {
		return roomToStateEvents(this);
	}
}

class MXEvent {
	constructor(content, homeserver, eventType) {
		this.props = content;
		this.homeserver = homeserver;
		this.user = `@admin:${this.homeserver}`;
		this.room_id = buildRoomId({
			homeserver: this.homeserver,
			localPath: this.props.room.localPath,
		});
		this.event_id = buildEventId({
			homeserver: this.homeserver,
			localPath: this.props.room.localPath,
			localAlias: this.props.localAlias,
		});
		this.date = Date.now();
		this.type = eventType || "m.room.message";
		this.msgtype = "m.text";
	}
	toRoomMessage() {
		const content = this.props.file.content;
		return {
			type: this.type,
			content: {
				body: content,
				msgtype: this.msgtype,
			},
			origin_server_ts: this.date,
			room_id: this.room_id,
			sender: this.user,
			event_id: "$ReWOh80liP9giY8iWW3HeG3FmnM_O6xALfm2Bqbw6y0",
			user_id: this.user,
		};
	}
}

export { MXRoom, MXSpace, MXEvent, getServerInfo, contentFolderToMX };
