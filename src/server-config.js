import { readFile, readdir, rm, mkdir } from "fs/promises";

// the static server config file, at the root of a users' project
const configFileName = "matrix-static.json";

// read the JSON content of the static server configuration file
const readConfig = async () => {
	let config = null,
		error = null;
	try {
		const configFile = await readFile(`./${configFileName}`, {
			encoding: "utf8",
		});
		config = JSON.parse(configFile);
	} catch (e) {
		error = e;
	}
	return { config, error };
};

const cleanOutputFolder = async (outputFolder) => {
	try {
		await rm(outputFolder, { recursive: true, force: true });
	} catch (err) {
		console.log(err);
	}
};

const createOutputFolder = async (outputFolder) => {
	try {
		await mkdir(`${outputFolder}`, { recursive: true });
	} catch (err) {
		console.log(err);
	}
};

export { readConfig, cleanOutputFolder, createOutputFolder };
