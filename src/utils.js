/* https://stackoverflow.com/a/34890276 */
const groupBy = function (list, key) {
	return list.reduce(function (grouped, item) {
		(grouped[item[key]] = grouped[item[key]] || []).push(item);
		return grouped;
	}, {});
};

export { groupBy };
