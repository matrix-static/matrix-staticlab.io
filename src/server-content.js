import recursive from "recursive-readdir";
import { readFile, writeFile } from "fs/promises";

// the static server config file, at the root of a users' project
const contentFolder = "content";
const buildFolder = "build";
const roomsFileDefinition = "index.json";

const getRoomFromPath = (roomPath) => {
	const pathItems = roomPath.split("/");
	const items = pathItems.slice(1, pathItems.length - 1);
	const localPath = items.join("/");
	return {
		localPath,
		localAlias: items[items.length - 1],
		parentSpace:
			items.length > 1 ? items.slice(0, items.length - 1).join("/") : null,
	};
};

const getEventFromPath = async (eventPath) => {
	let fileContent,
		room = getRoomFromPath(eventPath);

	const fileNamePath = eventPath.split("/");
	const localAlias = fileNamePath[fileNamePath.length - 1].split(".")[0];

	try {
		const eventFile = await readFile(`./${eventPath}`, {
			encoding: "utf8",
		});
		fileContent = JSON.parse(eventFile);
	} catch (e) {
		console.info(e);
	}
	return {
		file: fileContent,
		room,
		localAlias,
	};
};

// read the JSON content of the static server configuration file
const readContentFolder = async () => {
	let tree = null,
		error = null;
	try {
		tree = await recursive(`./${contentFolder}/`);
	} catch (e) {
		error = e;
	}

	const roomsPath = tree.filter((item) => item.endsWith(roomsFileDefinition));
	const eventsPath = tree.filter((item) => {
		const isJSON = item.endsWith(".json");
		const isRoom = roomsPath.includes(item);
		if (isJSON && !isRoom) {
			return item;
		}
	});

	const rooms = roomsPath
		.map((item) => {
			return getRoomFromPath(item);
		})
		.reduce((acc, item) => {
			acc[item.localPath] = item;
			return acc;
		}, {});

	const spaces = Object.keys(rooms).reduce((acc, item) => {
		const roomPath = item.split("/");
		const spacePathes = roomPath.slice(0, roomPath.length - 1);
		const spacePath = spacePathes.join("/");
		const localAlias = spacePathes[spacePathes.length - 1];
		const isSpace = spacePathes.length > 0;
		if (isSpace) {
			const content = {
				localPath: spacePath,
				localAlias,
				parentSpace:
					spacePathes.length > 1
						? spacePathes.slice(0, spacePathes.length - 1).join("/")
						: null,
			};
			acc[spacePath] = content;
		}
		return acc;
	}, {});

	let eventPromises = eventsPath.map(async (item) => {
		return getEventFromPath(item);
	});

	let events;
	try {
		events = await Promise.all(eventPromises).then((allEvents) => {
			return allEvents.reduce((acc, item) => {
				const eventPath = `${item.room.localPath}/${item.localAlias}`;
				acc[eventPath] = item;
				return acc;
			}, {});
		});
	} catch (e) {
		console.info(e);
	}

	return { rooms, spaces, events };
};

const readBuildFolder = async () => {
	let tree = null,
		error = null;
	try {
		const rawTree = await recursive(`./${buildFolder}/`);
		tree = rawTree.map((path) => {
			return path.replace(`${buildFolder}/`, "");
		});
	} catch (e) {
		error = e;
	}
	return { tree, error };
};

export { readContentFolder, readBuildFolder };
